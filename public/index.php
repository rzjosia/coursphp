<?php
session_start();
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cours PHP</title>
</head>

<body>

    <?php
    if (!isset($_SESSION["logged"])) :
    ?>

        <form action="traitement.php" method="POST">
            <input type="email" name="email" id="email" placeholder="Votre email">
            <input type="password" name="mdp" id="mdp" placeholder="Votre mot de passe">
            <button type="submit">Se connecter</button>
        </form>

        <div id="message"></div>
        <canvas id="game" width="500" height="500"></canvas>
        <script src="js/canvas.js"></script>
    <?php else : ?>

        <p>Vous etes connecté en tant que <?php echo $_SESSION["mail"]; ?> </p>
        <p><a href="membre.php">page membre</a></p>

    <?php endif; ?>

</body>

</html>