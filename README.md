# CoursPHP

Prérequis:
- docker
- make

Installation avec docker et make
```shell script
make build
```

Démarrer le serveur
```shell script
make start
```

Arrêter le serveur
```shell script
make stop
```

Liste des ports:
- **8989**; site web
- **8984**: phpmyadmin