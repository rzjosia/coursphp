project = coursphp
compose = docker-compose -p $(project)
exec = $(compose) exec

all: build start

build :
	@$(compose) build

rebuild: stop build

start-container:
	@$(compose) up

start: start-container

stop:
	@$(compose) stop
	@true

restart: stop start

clean:
	@docker container prune --filter "until=12h"

exec:
	@$(exec) web bash

sql:
	@$(exec) database bash

